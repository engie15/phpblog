<!-- Note the uppercase Database vs database.php -->
<!-- The uppercase at the start of a filename indicates in PHP that the file is a class -->

<?php
class Database {
    public $host = DB_HOST;
    public $username = DB_USER;
    public $password = DB_PASS;
    public $db_name = DB_NAME;

    public $link;
    public $error;

    // Class constructor
    public function __construct() {

        //Call connect Function
        $this->connect();
    }

    // Connector
    private function connect() {
        $this->link = new mysqli($this->host, $this->username, $this->password, $this->db_name);
        if (!$this->link) {
            $this->error = "Connection failed".$this->link->connect_erro;
            return false;
        }
    }

    // Select
    public function select($query) {
        $result = $this->link->query($query) or die($this->link->error.__LINE__);
        if($result->num_rows > 0) {
            return $result;
        } else {
            return false;
        }
    }


    //Insert
    public function insert($query) {
        $insert_row = $this->link->query($query) or die($this->link->error.__LINE__);

        //Validate Insert
        if($insert_row) {
            header("Location: index.php?msg=.urlencode('Record Added').");
            exit();
        } else {
            die('Error : ('.$this->link->erro .') '. $this->link->error);
        }
    }
    
    //update
    public function update($query) {
        $update_row = $this->link->query($query) or die($this->link->error.__LINE__);

        //Validate update
        if($update_row) {
            header("Location: index.php?msg=.urlencode('Record Updated').");
            exit();
        } else {
            die('Error : ('.$this->link->erro .') '. $this->link->error);
        }
    }
    
    //delete
    public function delete($query) {
        $delete_row = $this->link->query($query) or die($this->link->error.__LINE__);

        //Validate delete
        if($delete_row) {
            header("Location: index.php?msg=.urlencode('Record Deleted').");
            exit();
        } else {
            die('Error : ('.$this->link->erro .') '. $this->link->error);
        }
    }

}
