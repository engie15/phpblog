<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>My Te Ururangi Journey</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">
      <header class="blog-header py-3">
        <div class="row flex-nowrap justify-content-between align-items-center">
          <div class="col-12 text-center">
            <a class="blog-header-logo text-dark" href="#">Toku Haerenga Te Ururangi</a>
          </div>
        </div>
      </header>

      <div class="nav-scroller py-1 mb-2">
        <nav class="nav d-flex justify-content-between">
          <a class="p-2 text-muted" href="#"></a>
          <a class="p-2 text-muted" href="#"></a>
          <a class="p-2 text-muted" href="#"></a>
          <a class="p-2 text-muted" href="#"></a>
          <a class="p-2 text-muted" href="#"></a>
          <a class="p-2 text-muted" href="index.php">Home</a>
          <a class="p-2 text-muted" href="posts.php">Posts</a>
          <a class="p-2 text-muted" href="#"></a>
          <a class="p-2 text-muted" href="#"></a>
          <a class="p-2 text-muted" href="#"></a>
          <a class="p-2 text-muted" href="#"></a>
          <a class="p-2 text-muted" href="#"></a>
        </nav>
      </div>
