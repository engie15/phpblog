
        <aside class="col-md-4 blog-sidebar">
          <div class="p-3 mb-3 bg-light rounded">
            <h4 class="font-italic">About</h4>
            <p class="mb-0">
                Kia ora! Engie here (pronounced like the letters N, G - you got it!). Starting with zero coding or programming experience, my goal is to spend the next 10 years building my skills to become a full stack developer.
                <br/>
                Follow me and read about how a 30 year old Maori and CookIsland woman tackles her full immersive learning experience with EDA and takes her first step into te ao hangarau mamati.
            </p>
          </div>

          <div class="p-3">
            <h4 class="font-italic">Catergories</h4>
            <ol class="list-unstyled mb-0">
              <li><a href="#">Technical</a></li>
              <li><a href="#">Core</a></li>
            </ol>
          </div>

          <div class="p-3">
          </div>
        </aside><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </main><!-- /.container -->

    <footer class="blog-footer">
      <p>Toku Haerenga Te Ururangi &copy; 2017</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script> -->
    <script src="js/bootstrap.js"></script>
    <!-- <script src="../../assets/js/vendor/holder.min.js"></script>
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script> -->
  </body>
</html>