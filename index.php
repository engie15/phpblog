<?php include 'config/config.php' ?>
<?php include 'libraries/database.php'; ?>
<?php include 'includes/header.php'; ?>

<?php
  //Create DB Object
  $db = new Database();

  //Create query
  $query = "SELECT * from posts";

  //Run query
  $posts = $db->select($query);
?>


      <div class="jumbotron p-3 p-md-5 text-white rounded bg-info">
        <div class="col-md-6 px-0">
          <h1 class="display-4 font-italic">Title of a longer featured blog post</h1>
          <p class="lead my-3">Multiple lines of text that form the lede, informing new readers quickly and efficiently about what's most interesting in this post's contents.</p>
          <p class="lead mb-0"><a href="#" class="text-white font-weight-bold">Continue reading...</a></p>
        </div>
      </div>
<?php if($posts) : ?>
  <?php while($row = $posts->fetch_assoc()) : ?>
    <main role="main" class="container">
      <div class="row">
          <div class="blog-post">
            <h2 class="blog-post-title"><?php echo $row['title']; ?></h2>
            <p class="blog-post-meta">January 1, 2014 by <a href="#">Mark</a></p>
              <?php echo $row['body']; ?>
            <a href="post.php?id=1">Continue reading</a>
            <!-- <a class="readmore" href="post.php?id=1">Read More</a> -->
          </div><!-- /.blog-post -->
        </div><!-- /.blog-main -->
  <?php endwhile; ?>

<?php else : ?>
  <p>There are no posts yet</p>
<?php endif; ?>

<?php include 'includes/footer.php'; ?>